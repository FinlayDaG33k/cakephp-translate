# cake-translate
CakePHP plugin providing a shell command to translate things using LibreTranslate.  

**DISCLAIMER**: *This plugin is still under heavy development and should not be considered as ready for production. Use at your own risk*

## Installing
Install the library as follows:
> composer require finlaydag33k/cakephp-translate

Then enable it in your app by adding the following to your `src/Application`:
```php
public function bootstrap() {
  // ... Do other stuff here

  // Enable plugin
  $this->addPlugin('FinlayDaG33k/Translate');
}
```

## Usage
Translations can be done from the command line using the following command:
> cake translate <locale> <input language> <output language> [plugin]

An example to translate from `English` to `German`:
> cake translate de_DE en de

## Version Compatibility
Below a table of version compatibility.  
Please note that when a new version is released, support for older versions by the maintainer drop *immediately*.
| Plugin Version | CakePHP Version |
|----------------|-----------------|
| 1.x            | >=3.6.0         |