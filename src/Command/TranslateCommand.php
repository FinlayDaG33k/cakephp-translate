<?php
  namespace FinlayDaG33k\Translate\Command;

  use Cake\Console\Arguments;
  use Cake\Console\Command;
  use Cake\Console\ConsoleIo;
  use Cake\Console\ConsoleOptionParser;
  use Cake\Http\Client;
  use Sepia\PoParser\{
    SourceHandler\FileSystem,
    Parser,
    PoCompiler
  };
  use Cake\Core\Plugin;
  use Cake\Filesystem\File;

  class TranslateCommand extends Command {
    protected $apiUrl = 'https://translate.finlaydag33k.nl/translate';
    protected $http;

    public function initialize() {
      parent::initialize();

      /// Initialize the HTTP client
      $this->http = new Client();
    }

    protected function buildOptionParser(ConsoleOptionParser $parser) {
      $parser
        ->addArgument('locale', [
          'help' => 'locale file to use',
          'required' => true
        ])
        ->addArgument('source', [
          'help' => 'Source language (ISO-3166 ALPHA-2 code)',
          'required' => true
        ])
        ->addArgument('target', [
          'help' => 'target language (ISO-3166 ALPHA-2 code)',
          'required' => true
        ])
        ->addArgument('plugin', [
          'help' => 'Plugin to translate',
          'required' => false,
          'short' => 'p'
        ]);

      return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io) {
      // Get the path to the default po file
      if(empty($args->getArgument('plugin'))) {
        $input = APP . 'Locale' . DS . 'default.po';
        $output = APP . 'Locale' . DS . $args->getArgument('locale') . DS . 'default.po';
      } else {
        // Check if the plugin exists
        if(!Plugin::loaded($args->getArgument('plugin'))) {
          $io->error('Plugin "' . $args->getArgument('plugin'). '" is not loaded.');
          $this->abort();
        }

        // Get the plugin simple name
        $name = explode('/', $args->getArgument('plugin'));
        if(count($name) > 1) {
          $name = lcfirst($name[1]);
        } else {
          $name = lcfirst($name[0]);
        }

        // Get path to the potfiles
        $input = Plugin::path($args->getArgument('plugin')) . 'src' . DS . 'Locale' . DS . 'default.po';
        $output = Plugin::path($args->getArgument('plugin')) . 'src' . DS .'Locale' . DS . $args->getArgument('locale') . DS . $name . '.po';
      }

      // Check if the file exists
      $file = new File($input);
      if(!$file->exists()) {
        $io->error('Potfile doesn\'t exist at "'.$input.'"');
        $this->abort();
      }

      // Open the file
      $io->out("Opening file at " . $input . "... ", 0);
      try {
        $fileHandler = new FileSystem($input);
      } catch(\Exception $e) {
        $io->out();
        $io->err("Could not open file!");
        $io->err($e);
        $this->abort();
      }
      $io->out('OK');

      // Parse the file
      $io->out("Parsing pot file... ", 0);
      try {
        $poParser = new Parser($fileHandler);
        $catalog = $poParser->parse();
      } catch(\Exception $e) {
        $io->out();
        $io->err("Failure while parsing...");
        $io->err($e);
        $this->abort();
      }
      $io->out('OK');

      // Count the amount of entries
      $entryCount = count($catalog->getEntries());

      // Translate all entries using the LibreTranslate API
      $current = 1;
      $score = [
        'success' => 0,
        'failure' => 0,
        'skipped' => 0
      ];
      $io->out("Starting translation of " . $entryCount . " entries from \"".$args->getArgument('source')."\" to \"".$args->getArgument('target')."\"...");
      foreach($catalog->getEntries() as &$entry) {
        $io->out("Translating entry #" . $current . " of " . $entryCount . "... ", 0);

        // Check if there is already a translation
        // Skip it if there is
        if(!empty($entry->getMsgStr())) {
          $current++;
          $score['skipped']++;
          $io->out('SKIP');
          continue;
        }
        
        // Make the request to the api
        try {
          $translation = $this->translateEntry(
            $entry->getMsgId(),
            $args->getArgument('source'),
            $args->getArgument('target')
          );

          // Check if there are any errors
          // Throw an exception if so
          if(!empty($translation['error'])) throw new \Exception($translation['error']);
          if(empty($translation['translatedText'])) throw new \Exception('No translation found');

          $translated = $translation['translatedText'];
          $entry->setMsgStr($translated);
          $score['success']++;
          $io->out("OK");
        } catch(\Exception $e) {
          $score['failure']++;
          $io->out("FAIL (" . $e->getMessage() . ")");
        }

        $current++;
      }

      // We've finished translating
      $io->out("Successfully translated " . $score['success'] . ' out of ' . $entryCount . ' entries with ' . $score['failure'] . ' failures (' . $score['skipped'] . ' skipped entries)');

      // Save the translated file
      $io->out('Saving file... ', 0);
      try {
        // Compile the new file
        $compiler = new PoCompiler();
        $compiled = $compiler->compile($catalog);

        // Save it to the specified locale
        $file = new File($output, true);
        $file->write($compiled);
        
        $io->out('OK');
      } catch(\Exception $e) {
        $io->out();
        $io->err("Failed saving file!");
        $io->err($e);
        $this->abort();
      }
    }

    private function translateEntry(string $message, string $source, string $target) {
      $response = $this->http->post(
        $this->apiUrl,
        json_encode([
          'q' => $message,
          'source' => $source,
          'target' => $target,
        ]),
        ['headers' => [
          'Content-Type' => 'application/json',
        ]]
      );
      return json_decode($response->getStringBody(), 1);
    }
  }