<?php
  namespace FinlayDaG33k\Translate;

  use Cake\Core\BasePlugin;
  use Cake\Core\PluginApplicationInterface;
  use FinlayDaG33k\Translate\Command\TranslateCommand;
  
  class Plugin extends BasePlugin {
    public function console($commands) {
      // Add console commands here.
      $commands = parent::console($commands);
  
      $commands->add('translate', TranslateCommand::class);

      return $commands;
    }
  }